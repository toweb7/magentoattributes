<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', 'AppController@index');
Route::any('/product_attribute', 'AppController@productAttribute');
Route::any('/category_attribute', 'AppController@categoryAttribute');
Route::get('/about_project', 'AppController@aboutProject');
