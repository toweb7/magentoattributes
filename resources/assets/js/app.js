jQuery(document).ready(function ($) {

    checkAttributeTypeDropdown();

    $('#attributeType').change(function(){

        checkAttributeTypeDropdown();
    })

    function checkAttributeTypeDropdown() {
        var attributeType = $('#attributeType').val();
        var attributeOptionsFiled = $('#attributeOptions');

        if (attributeType == 'select' || attributeType == 'multiselect') {
            attributeOptionsFiled.show();
        } else {
            attributeOptionsFiled.find('textarea').val('');
            attributeOptionsFiled.hide();
        }
    }
});