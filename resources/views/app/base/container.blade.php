<!DOCTYPE html>
<html lang="en">
<head>
	<title>Programatically create magento product and catalog attribute on fly, online</title>
	<meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<meta name="description" content="Programatically create product and catalog attribute in Magento on fly, online." />
	<meta name="keywords" content="magento, attribute, programatically , create, generator, scandiweb.com" />
	<meta name="robots" content="INDEX,FOLLOW" />

	<link href="{{ asset('/css/app.css') }}" rel="stylesheet">

	@yield('head')
</head>
<body>
	@yield('content')

	<!-- Scripts -->
	<script type="text/javascript" src="{{ asset('js/all.js') }}"></script>
	<script>
		(function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
					(i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
				m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
		})(window,document,'script','https://www.google-analytics.com/analytics.js','ga');

		ga('create', 'UA-92005052-1', 'auto');
		ga('send', 'pageview');

	</script>
	<script type="text/javascript">
		window._urq = window._urq || [];
		_urq.push(['initSite', 'd7789ccd-0d2a-465d-985b-98528d220b7a']);
		(function() {
			var ur = document.createElement('script'); ur.type = 'text/javascript'; ur.async = true;
			ur.src = ('https:' == document.location.protocol ? 'https://cdn.userreport.com/userreport.js' : 'http://cdn.userreport.com/userreport.js');
			var s = document.getElementsByTagName('script')[0]; s.parentNode.insertBefore(ur, s);
		})();
	</script>
</body>
</html>