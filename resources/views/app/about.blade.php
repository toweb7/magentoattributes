@extends('app.base.container')

@section('content')
    @include('app.base.navbar')

    <div class="container-fluid">
        <div class="row">
            <div class="col-md-6">
                <p>I hope this small app should simplify the daily Magento Developer task to create the migration script where is required to create new Product, Category attribute.</p>
                <p>I did small deep Magento investigation and prepared the best solution how to generate short\correct snippet to easy create the new Magento attributes.</p>
                <p>If you have ideas how to improve it, please write me to <a href="mailto:aleksandrs@scandiweb.com">aleksandrs@scandiweb.com</a>.</p>
                <p>If my work helped to you, please donate.</p>
                <form action="https://www.paypal.com/cgi-bin/webscr" method="post" target="_top">
                    <input type="hidden" name="cmd" value="_s-xclick">
                    <input type="hidden" name="hosted_button_id" value="VPP36B23F657L">
                    <input type="image" src="https://www.paypalobjects.com/en_US/i/btn/btn_donateCC_LG.gif" border="0" name="submit" alt="PayPal - The safer, easier way to pay online!">
                    <img alt="" border="0" src="https://www.paypalobjects.com/en_US/i/scr/pixel.gif" width="1" height="1">
                </form>
            </div>
        </div>
    </div>
@endsection