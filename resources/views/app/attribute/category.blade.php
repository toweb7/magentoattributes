@extends('app.base.container')

@section('content')
    @include('app.base.navbar')

    <div class="container-fluid">
        <div class="row">
            <div class="col-md-5">
                <form class="form-horizontal" method="post" action="{{ url('category_attribute') }}">
                    <input type="hidden" name="_token" value="{{ csrf_token() }}">
                    <div class="form-group">
                        <label class="col-sm-3 control-label">Code</label>
                        <div class="col-sm-9">
                            <input name="code" value="{{ $attributeData['code'] }}" placeholder="e.g. some_test_attribute" type="text" class="form-control" required="true">
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-sm-3 control-label">Group</label>
                        <div class="col-sm-9">
                            <input name="group" value="{{ $attributeData['group'] }}" placeholder="e.g. General" type="text" class="form-control" required="true">
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-sm-3 control-label">Label</label>
                        <div class="col-sm-9">
                            <input name="label" value="{{ $attributeData['label'] }}" placeholder="e.g. Test Attribute" type="text" class="form-control" required="true">
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-sm-3 control-label">Scope</label>
                        <div class="col-sm-9">
                            <select name="global" class="form-control" required="true">
                                <option value="1" {{ ($attributeData['global'] == '1') ? 'selected="selected"' : '' }}>Global</option>
                                <option value="2" {{ ($attributeData['global'] == '2') ? 'selected="selected"' : '' }}>Website</option>
                                <option value="0" {{ ($attributeData['global'] == '0') ? 'selected="selected"' : '' }}>Store View</option>
                            </select>
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-sm-3 control-label">Type</label>
                        <div class="col-sm-3">
                            <select id="attributeType" name="input" class="form-control" required="true">
                                <?php foreach ($attributeTypes as $attributeType => $label): ?>
                                <option value="{{ $attributeType }}" {{ ($attributeData['input'] == $attributeType) ? 'selected="selected"' : '' }}>{{ $label }}</option>
                                <?php endforeach; ?>
                            </select>
                        </div>
                    </div>
                    <div id="attributeOptions" class="form-group" style="{{ empty($attributeData['option']['values']) ? 'display: none;' : '' }}">
                        <label class="col-sm-3 control-label">Dropdown Options</label>
                        <div class="col-sm-9">
                            <textarea name="option[values]" rows="7" class="form-control">{{ !empty($attributeData['option']['values']) ? implode("\n", $attributeData['option']['values']) : '' }}</textarea>
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-sm-3 control-label">Default</label>
                        <div class="col-sm-9">
                            <input name="default" value="{{ $attributeData['default'] }}" type="text" class="form-control">
                        </div>
                    </div>

                    <div class="form-group">
                        <label class="col-sm-3 control-label">Unique</label>
                        <div class="col-sm-9">
                            <select name="unique" class="form-control" required="true">
                                <option value="0" {{ ($attributeData['unique'] == '0') ? 'selected="selected"' : '' }}>No</option>
                                <option value="1" {{ ($attributeData['unique'] == '1') ? 'selected="selected"' : '' }}>Yes</option>
                            </select>
                        </div>
                    </div>

                    <div class="form-group">
                        <label class="col-sm-3 control-label">Required</label>
                        <div class="col-sm-9">
                            <select name="required" class="form-control" required="true">
                                <option value="0" {{ ($attributeData['required'] == '0') ? 'selected="selected"' : '' }}>No</option>
                                <option value="1" {{ ($attributeData['required'] == '1') ? 'selected="selected"' : '' }}>Yes</option>
                            </select>
                        </div>
                    </div>

                    <div class="form-group">
                        <label class="col-sm-3 control-label">Visible</label>
                        <div class="col-sm-9">
                            <select name="visible" class="form-control" required="true">
                                <option value="0" {{ ($attributeData['visible'] == '0') ? 'selected="selected"' : '' }}>No</option>
                                <option value="1" {{ ($attributeData['visible'] == '1') ? 'selected="selected"' : '' }}>Yes</option>
                            </select>
                        </div>
                    </div>

                    <div class="form-group">
                        <div class="col-sm-offset-3 col-sm-9">
                            <button type="reset" class="btn btn-lg btn-danger">Reset</button>
                            <button type="submit" class="btn btn-lg btn-primary">Generate</button>
                        </div>
                    </div>
                </form>
            </div>
            <div class="col-md-7">
                <div class="alert alert-info" role="alert" >NOTE: To make working "$this->addAttribute" in your migration script you should use the setup class Mage_Sales_Model_Resource_Setup</div>
<pre>
/* @var $installer Mage_Sales_Model_Resource_Setup */
$installer = $this;

$installer->startSetup();

$this->addAttribute('{{ $attributeData['entity_type'] or '...' }}', '{{ $attributeData['code'] or '...' }}', array(
    'group' => '{{ $attributeData['group'] or '...' }}',
    'label' => '{{ $attributeData['label'] or '...' }}',
    'global' => {{ $attributeData['global'] or '...' }},

    'input' => '{{ $attributeData['input'] or 'text' }}',
<?php if (!empty($attributeData['option']['values'])): ?>
    'option'   => array(
        'values' => array(
<?php foreach ($attributeData['option']['values'] as $option): ?>
            '{{ trim($option) }}',
<?php endforeach; ?>
        ),
    ),
<?php endif; ?>

    'required' => '{{ $attributeData['required'] }}',
    'unique' => '{{ $attributeData['unique'] }}',
<?php if ($attributeData['default']): ?>
    'default' => '{{ $attributeData['default'] }}',
<?php endif; ?>

    'visible' => '{{ $attributeData['visible'] }}'
));

$installer->endSetup();
</pre>
                <form action="https://www.paypal.com/cgi-bin/webscr" method="post" target="_top">
                    <input type="hidden" name="cmd" value="_s-xclick">
                    <input type="hidden" name="hosted_button_id" value="VPP36B23F657L">
                    <input type="image" src="https://www.paypalobjects.com/en_US/i/btn/btn_donateCC_LG.gif" border="0" name="submit" alt="PayPal - The safer, easier way to pay online!">
                    <img alt="" border="0" src="https://www.paypalobjects.com/en_US/i/scr/pixel.gif" width="1" height="1">
                </form>
            </div>
        </div>
    </div>
@endsection