<?php
namespace App\Http\Controllers;

use Illuminate\Http\Request;

class AppController extends Controller
{
    public function index()
    {
        return redirect('product_attribute');
    }

    /**
     * Generate Product Attribute
     *
     * @param Request $request
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function productAttribute(Request $request)
    {
        //Prepare form default values
        $attributeData = array(
            'entity_type' => 'catalog_product',
            'code' => 'test_attribute',

            'group' => 'General',
            'label' => 'Test Attribute',
            'global' => '1',

            'input' => 'text',
            'option' => ['values' => []],

            'required' => '0',
            'unique' => '0',
            'default' => '',

            'visible' => '1',

            'apply_to' => ['simple', 'grouped', 'configurable', 'virtual', 'bundle', 'downloadable']
        );

        //Do modifications with input
        $attributesDataInput = $request->all();
        if (!empty($attributesDataInput['option']['values'])) {
            $attributesDataInput['option']['values'] = explode("\n", $attributesDataInput['option']['values']);
        }

        $attributeData = array_merge($attributeData, $attributesDataInput);

        //Convert apply to
        $attributeData['apply_to'] = implode(', ', $attributeData['apply_to']);

        return view('app.attribute.product', [
            'attributeData' => $attributeData,
            'attributeTypes' => [
                'text' => 'Text Field',
                'textarea' => 'Text Area',
                'date' => 'Date',
                'boolean' => 'Yes/No',
                'multiselect' => 'Multiple Select',
                'select' => 'Dropdown',
                'price' => 'Price',
                'media_image' => 'Media Image'
            ]
        ]);
    }

    /**
     * Generate Category Attribute
     *
     * @param Request $request
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function categoryAttribute(Request $request)
    {
        //Prepare form default values
        $attributeData = array(
            'entity_type' => 'catalog_category',
            'code' => 'test_attribute',

            'group' => 'General Information',
            'label' => 'Test Attribute',
            'global' => '1',

            'input' => 'text',
            'option' => ['values' => []],

            'required' => '0',
            'unique' => '0',
            'default' => '',

            'visible' => '1'
        );

        //Do modifications with input
        $attributesDataInput = $request->all();
        if (!empty($attributesDataInput['option']['values'])) {
            $attributesDataInput['option']['values'] = explode("\n", $attributesDataInput['option']['values']);
        }

        $attributeData = array_merge($attributeData, $attributesDataInput);

        return view('app.attribute.category', [
            'attributeData' => $attributeData,
            'attributeTypes' => [
                'text' => 'Text Field',
                'textarea' => 'Text Area',
                'date' => 'Date',
                'boolean' => 'Yes/No',
                'multiselect' => 'Multiple Select',
                'select' => 'Dropdown',
                'image' => 'Image'
            ]
        ]);
    }

    public function aboutProject()
    {
        return view('app.about');
    }
}