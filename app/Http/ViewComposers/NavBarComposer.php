<?php namespace App\Http\ViewComposers;

use Illuminate\View\View;

class NavBarComposer
{
    /**
     * Bind data to the view.
     *
     * @param  View $view
     * @return void
     */
    public function compose(View $view)
    {
        $navigationLinks = [
            'Product Attribute' => 'product_attribute',
            'Category Attribute' => 'category_attribute',
            'About' => 'about_project'
        ];

        $view->with([
            'navigationLinks' => $navigationLinks,
            'defaultSection' => 'product_attribute',
            'pageTitle' => 'Magento Attributes'
        ]);
    }
}